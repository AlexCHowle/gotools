# retry

A simple Golang function execution retrier that enables an arbitrary function to be configured for retry on error. Retries are executed at a bounded random interval to provide jitter.

## API

### Variables

* `MaxRetries` - the maximum number of retry attempts before giving up.
* `MinWait` - the minimum duration to wait before attempting a retry. Default is 250ms.
* `MaxWait` - the maximum duration to wait before attempting a retry. Default is 500ms.

### Types

* `retry.Command` - Represents the function to be retried, along with the retry parameters.
  * `Function` (`func() (interface{}, error)`) - the function to execute.
  * `MaxRetries` (`int`) - the maximum number of retry attempts to perform.
  * `MinWait` (`time.Duration`) - the minimum amount of time to wait before attempting the retry.
  * `MaxWait` (`time.Duration`) - the maximum amount of time to wait before attempting the retry.
  * `Outputter` (`io.Writer`) - an (optional) output stream for logging retry attempts.

### Functions

* `retry.ExecuteFunc(f func() (interface{}, error)) (interface{}, error)` - executes the provided function. This will retry 4 times at random intervals between 250ms and 500ms. Returns the successful response, or any final `error`.
* `retry.Execute(cmd retry.Command) (interface{}, error)` - executes the `retry.Command` and retries as configured. Returns the successful response, or any final `error`.

## Examples

### ExecuteFunc(func() (interface{}, error))

```go
getResult, err := retry.ExecuteFunc(func() (interface{}, error) {
    return http.Get("http://some-flaky-endpoint")
})


// Change the defaults for all future invocations of ExecuteFunc()
retry.MaxRetries = 2
retry.MinWait = 1000 * time.Millisecond
retry.MaxWait = 2000 * time.Millisecond

```

### Execute(retry.Command)

```go
getFunc := func() (interface{}, error) {
    return http.Get("http://some-flaky-endpoint")
}
cmd := retry.Command{
    MaxRetries: 2,
    MinWait:    100,
    MaxWait:    200,
    Function:   getFunc,
}
getResult, err := retry.Execute(cmd)
```

## Remarks

* Whether to retry or not can be influenced by the behaviour of the `error` type returned by the executed `Function`. If the `error` type implements a `Retry() bool` method, the body of said `Retry()` method will be evaluated. For example:

```go
type noRetryError struct {
    error
}

func (nre noRetryError) Retry() bool { return false } // this is evaluated by the retry package

myFunc := func() (interface{}, error) {
    result, err := doSomeNonRetriableWork()
    if err != nil {
        return nil, noRetryError{error: err}
    }

    result, err := doRetriableWork()
    if err != nil {
        return nil, err // assuming err does not implement "Retry() bool" method
    }
}

result, err := retry.ExecuteFunc(myFunc)
```

* Study `../cmd/retrytester/retrytester.go` for concrete examples.